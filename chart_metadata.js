var barCanvasAbstract = document.getElementById("bar-chart-abstract");
var barCanvasAffiliation = document.getElementById("bar-chart-affiliation");
var barCanvasAuthor = document.getElementById("bar-chart-author");
var barCanvasTitle = document.getElementById("bar-chart-title");
var barCanvasSource = document.getElementById("bar-chart-source");
var barCanvasYear = document.getElementById("bar-chart-year");

Chart.defaults.global.defaultFontFamily = "Times New Roman";
Chart.defaults.global.defaultFontSize = 15;
Chart.defaults.global.defaultFontColor = "black";

var barDataAbstract = {
  labels: ['Mendeley Desktop', 'GROBID', 'SVMHeaderParse', 'ParsCit', 'CERMINE', 'Cb2Bib'],
  datasets: [{
  	label: "Value",
        backgroundColor: "#424bf4",
    data: [0.7064, 0.7384, 0.6574, 0.7011, 0.7068, 0.5591],
  }]
};

var descrAbstract = {
'Mendeley Desktop': "Available for Windows, Mac and Linux, Mendeley Desktop lets you save PDFs, generate citations, organize references, and automatically backup and sync your files across different computers. Mendeley Desktop is not open-source and that was the reason why we did not integrate the test function into our website. To get more information about the tool and download it click on the link below.<p><a href='https://www.mendeley.com/download-desktop/'>Link to the website of Mendeley Desktop</a></p>",
'GROBID': "GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents.<p><a href='http://grobid.readthedocs.io/en/latest/Introduction/'>Link to the official documentation</a></p><p><a href='grobid.html'>Try the tool</a></p>",
'SVMHeaderParse': "SVMHeaderParse is one of the algorithms of the CiteSeerExtractor for metadata extraction from PDF. You can use RESTful API or run your own server for production purposes. But if you just want to try out the tool, click on the link below.<p><a href='http://citeseerextractor.ist.psu.edu:8080/extractor'>Link to the website of SVMHeaderParse</a></p>",
'ParsCit': "ParsCit parses header metadata and citations of scientific articles in PDF.<p><a href='http://parscit.comp.nus.edu.sg/#ws'>Here you can try the tool and find additional information about it.</a></p>",
'CERMINE': "CERMINE is a Java library and a web-service for extracting metadata and content from scientific articles in born digital form.<p><a href='http://cermine.ceon.pl/index.html'>Link to the official website</a></p> <p><a href='cermine.html'>Try the tool</a></p>",
'Cb2Bib': "The cb2Bib is free, open-source and multiplatform application for metadata extraction from PDF. The application is GUI-based and cannot be used as command-line utlitity. That is why it is not possible to try the tool on this website. To get more information about the tool click on the link below.<p><a href='http://www.molspaces.com/d_cb2bib-overview.php'>Link to the website of cb2Bib</a></p>"
};

var barDataAffiliation = {
  labels: ['GROBID', 'SVMHeaderParse', 'ParsCit', 'CERMINE'],
  datasets: [{
  	label: "Value",
		backgroundColor: "#ff0000",
    data: [0.5512, 0.6432, 0.6352, 0.4210],
  }]
};

var descrAffiliation = {
'GROBID': "GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents.<p><a href='http://grobid.readthedocs.io/en/latest/Introduction/'>Link to the official documentation</a></p><p><a href='grobid.html'>Try the tool</a></p>",
'SVMHeaderParse': "SVMHeaderParse is one of the algorithms of the CiteSeerExtractor for metadata extraction from PDF. You can use RESTful API or run your own server for production purposes. But if you just want to try out the tool, click on the link below.<p><a href='http://citeseerextractor.ist.psu.edu:8080/extractor'>Link to the website of SVMHeaderParse</a></p>",
'ParsCit': "ParsCit parses header metadata and citations of scientific articles in PDF.<p><a href='http://parscit.comp.nus.edu.sg/#ws'>Here you can try the tool and find additional information about it.</a></p>",
'CERMINE': "CERMINE is a Java library and a web-service for extracting metadata and content from scientific articles in born digital form.<p><a href='http://cermine.ceon.pl/index.html'>Link to the official website</a></p> <p><a href='cermine.html'>Try the tool</a></p>"
};

var barDataAuthor = {
  labels: ['Mendeley Desktop', 'GROBID', 'SVMHeaderParse', 'ParsCit', 'CERMINE', 'Cb2Bib'],
  datasets: [{
    label: "Value",
    backgroundColor: "#0000ff",
    data: [0.8546, 0.9313, 0.7648, 0.8350, 0.6698, 0.9132],
  }]
};

var descrAuthor = {
'Mendeley Desktop': "Available for Windows, Mac and Linux, Mendeley Desktop lets you save PDFs, generate citations, organize references, and automatically backup and sync your files across different computers. Mendeley Desktop is not open-source and that was the reason why we did not integrate the test function into our website. To get more information about the tool and download it click on the link below.<p><a href='https://www.mendeley.com/download-desktop/'>Link to the website of Mendeley Desktop</a></p>",
'GROBID': "GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents.<p><a href='http://grobid.readthedocs.io/en/latest/Introduction/'>Link to the official documentation</a></p><p><a href='grobid.html'>Try the tool</a></p>",
'SVMHeaderParse': "SVMHeaderParse is one of the algorithms of the CiteSeerExtractor for metadata extraction from PDF. You can use RESTful API or run your own server for production purposes. But if you just want to try out the tool, click on the link below.<p><a href='http://citeseerextractor.ist.psu.edu:8080/extractor'>Link to the website of SVMHeaderParse</a></p>",
'ParsCit': "ParsCit parses header metadata and citations of scientific articles in PDF.<p><a href='http://parscit.comp.nus.edu.sg/#ws'>Here you can try the tool and find additional information about it.</a></p>",
'CERMINE': "CERMINE is a Java library and a web-service for extracting metadata and content from scientific articles in born digital form.<p><a href='http://cermine.ceon.pl/index.html'>Link to the official website</a></p> <p><a href='cermine.html'>Try the tool</a></p>",
'Cb2Bib': "The cb2Bib is free, open-source and multiplatform application for metadata extraction from PDF. The application is GUI-based and cannot be used as command-line utlitity. That is why it is not possible to try the tool on this website. To get more information about the tool click on the link below.<p><a href='http://www.molspaces.com/d_cb2bib-overview.php'>Link to the website of cb2Bib</a></p>"
};

var barDataTitle = {
  labels: ['Mendeley Desktop', 'GROBID', 'SVMHeaderParse', 'ParsCit', 'CERMINE', 'Docsplit', 'Cb2Bib'],
  datasets: [{
    label: "Value",
    backgroundColor: "#ff9900",
    data: [0.9130, 0.7713, 0.6187, 0.8823, 0.7491, 0.0, 0.0094],
  }]
};

var descrTitle = {
'Mendeley Desktop': "Available for Windows, Mac and Linux, Mendeley Desktop lets you save PDFs, generate citations, organize references, and automatically backup and sync your files across different computers. Mendeley Desktop is not open-source and that was the reason why we did not integrate the test function into our website. To get more information about the tool and download it click on the link below.<p><a href='https://www.mendeley.com/download-desktop/'>Link to the website of Mendeley Desktop</a></p>",
'GROBID': "GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents.<p><a href='http://grobid.readthedocs.io/en/latest/Introduction/'>Link to the official documentation</a></p><p><a href='grobid.html'>Try the tool</a></p>",
'SVMHeaderParse': "SVMHeaderParse is one of the algorithms of the CiteSeerExtractor for metadata extraction from PDF. You can use RESTful API or run your own server for production purposes. But if you just want to try out the tool, click on the link below.<p><a href='http://citeseerextractor.ist.psu.edu:8080/extractor'>Link to the website of SVMHeaderParse</a></p>",
'ParsCit': "ParsCit parses header metadata and citations of scientific articles in PDF.<p><a href='http://parscit.comp.nus.edu.sg/#ws'>Here you can try the tool and find additional information about it.</a></p>",
'CERMINE': "CERMINE is a Java library and a web-service for extracting metadata and content from scientific articles in born digital form.<p><a href='http://cermine.ceon.pl/index.html'>Link to the official website</a></p> <p><a href='cermine.html'>Try the tool</a></p>",
"Docsplit": "Docsplit is a command-line utility which is capable of extracting text and header metadata: year, title and authors. <p><a href='https://documentcloud.github.io/docsplit/'>Link to the official documentation</a></p><p><a href='docsplit.html'>Try the tool</a></p>",
'Cb2Bib': "The cb2Bib is free, open-source and multiplatform application for metadata extraction from PDF. The application is GUI-based and cannot be used as command-line utlitity. That is why it is not possible to try the tool on this website. To get more information about the tool click on the link below.<p><a href='http://www.molspaces.com/d_cb2bib-overview.php'>Link to the website of cb2Bib</a></p>"
};

var barDataYear = {
  labels: ['Mendeley Desktop', 'GROBID', 'SVMHeaderParse', 'CERMINE', 'Cb2Bib', 'Docsplit'],
  datasets: [{
    label: "Value",
    backgroundColor: "#00ff00",
    data: [0.68, 0.0830, 0.05, 0.17, 0.16, 0.0],
  }]
};

var descrYear = {
'Mendeley Desktop': "Available for Windows, Mac and Linux, Mendeley Desktop lets you save PDFs, generate citations, organize references, and automatically backup and sync your files across different computers. Mendeley Desktop is not open-source and that was the reason why we did not integrate the test function into our website. To get more information about the tool and download it click on the link below.<p><a href='https://www.mendeley.com/download-desktop/'>Link to the website of Mendeley Desktop</a></p>",
'GROBID': "GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents.<p><a href='http://grobid.readthedocs.io/en/latest/Introduction/'>Link to the official documentation</a></p><p><a href='grobid.html'>Try the tool</a></p>",
'SVMHeaderParse': "SVMHeaderParse is one of the algorithms of the CiteSeerExtractor for metadata extraction from PDF. You can use RESTful API or run your own server for production purposes. But if you just want to try out the tool, click on the link below.<p><a href='http://citeseerextractor.ist.psu.edu:8080/extractor'>Link to the website of SVMHeaderParse</a></p>",
'CERMINE': "CERMINE is a Java library and a web-service for extracting metadata and content from scientific articles in born digital form.<p><a href='http://cermine.ceon.pl/index.html'>Link to the official website</a></p> <p><a href='cermine.html'>Try the tool</a></p>",
'Cb2Bib': "The cb2Bib is free, open-source and multiplatform application for metadata extraction from PDF. The application is GUI-based and cannot be used as command-line utlitity. That is why it is not possible to try the tool on this website. To get more information about the tool click on the link below.<p><a href='http://www.molspaces.com/d_cb2bib-overview.php'>Link to the website of cb2Bib</a></p>",
'Docsplit': "Docsplit is a command-line utility which is capable of extracting text and header metadata: year, title and authors. <p><a href='https://documentcloud.github.io/docsplit/'>Link to the official documentation</a></p><p><a href='docsplit.html'>Try the tool</a></p>"
};

var barDataSource = {
  labels: ['Mendeley Desktop','CERMINE', 'Cb2Bib'],
  datasets: [{
    label: "Value",
    backgroundColor: "#00ffff",
    data: [0.0619, 0.0280, 0.0206],
  }]
};

var descrSource = {
'Mendeley Desktop': "Available for Windows, Mac and Linux, Mendeley Desktop lets you save PDFs, generate citations, organize references, and automatically backup and sync your files across different computers. Mendeley Desktop is not open-source and that was the reason why we did not integrate the test function into our website. To get more information about the tool and download it click on the link below.<p><a href='https://www.mendeley.com/download-desktop/'>Link to the website of Mendeley Desktop</a></p>",
'CERMINE': "CERMINE is a Java library and a web-service for extracting metadata and content from scientific articles in born digital form.<p><a href='http://cermine.ceon.pl/index.html'>Link to the official website</a></p> <p><a href='cermine.html'>Try the tool</a></p>",
'Cb2Bib': "The cb2Bib is free, open-source and multiplatform application for metadata extraction from PDF. The application is GUI-based and cannot be used as command-line utlitity. That is why it is not possible to try the tool on this website. To get more information about the tool click on the link below.<p><a href='http://www.molspaces.com/d_cb2bib-overview.php'>Link to the website of cb2Bib</a></p>"
};

var barOptions = {
	legend: {
  display: false,
  position: 'top',
  labels: {
  	boxWidth: 80,
    fontColor: 'black'
    }
  },
  scales: {
    yAxes: [{
    scaleLabel: {
      display: true,
      labelString: 'Score'
     }, 
     ticks: {
        max: 1,
        min: 0,
        stepSize: 0.2
      }
    }],
    xAxes: [{
      ticks: {
        minRotation: 45,
        maxRotation: 45,
        barPercentage: 0.8,
        autoSkip: false
      }
    }]
  }
};

var barChartAbstract = new Chart(barCanvasAbstract, {
	type: 'bar',
  data: barDataAbstract,
  options: barOptions
});

var barChartAffiliation = new Chart(barCanvasAffiliation, {
  type: 'bar',
  data: barDataAffiliation,
  options: barOptions
});

var barChartAuthor = new Chart(barCanvasAuthor, {
  type: 'bar',
  data: barDataAuthor,
  options: barOptions
});

var barChartTitle = new Chart(barCanvasTitle, {
  type: 'bar',
  data: barDataTitle,
  options: barOptions
});

var barChartYear = new Chart(barCanvasYear, {
  type: 'bar',
  data: barDataYear,
  options: barOptions
});

var barChartSource = new Chart(barCanvasSource, {
  type: 'bar',
  data: barDataSource,
  options: barOptions
});


barCanvasAbstract.onclick = function(evt) {
  var activePoints =  barChartAbstract.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
    var label = activePoints[0]._model.label;
     } catch (error) {
    console.warn("Not interactive element was clicked");
    }
$("#myModal-abstract .modal-body").html("");
$("#myModal-abstract .modal-title").html("");
console.log(descrAbstract[label]);
$("#myModal-abstract .modal-body").append("<p>" + descrAbstract[label] + "</p>");
$("#myModal-abstract .modal-title").append(label);
$("#myModal-abstract").modal("toggle");
};

barCanvasAffiliation.onclick = function(evt) {
  var activePoints =  barChartAffiliation.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
    var label = activePoints[0]._model.label;
     } catch (error) {
    console.warn("Not interactive element was clicked");
    }
$("#myModal-affiliation .modal-body").html("");
$("#myModal-affiliation .modal-title").html("");
console.log(descrAffiliation[label]);
$("#myModal-affiliation .modal-body").append("<p>" + descrAffiliation[label] + "</p>");
$("#myModal-affiliation .modal-title").append(label);
$("#myModal-affiliation").modal("toggle");
};

barCanvasAuthor.onclick = function(evt) {
  var activePoints =  barChartAuthor.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
    var label = activePoints[0]._model.label;
     } catch (error) {
    console.warn("Not interactive element was clicked");
    }
$("#myModal-author .modal-body").html("");
$("#myModal-author .modal-title").html("");
console.log(descrAuthor[label]);
$("#myModal-author .modal-body").append("<p>" + descrAuthor[label] + "</p>");
$("#myModal-author .modal-title").append(label);
$("#myModal-author").modal("toggle");
};

barCanvasTitle.onclick = function(evt) {
  var activePoints =  barChartTitle.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
    var label = activePoints[0]._model.label;
     } catch (error) {
    console.warn("Not interactive element was clicked");
    }
$("#myModal-title .modal-body").html("");
$("#myModal-title .modal-title").html("");
console.log(descrTitle[label]);
$("#myModal-title .modal-body").append("<p>" + descrTitle[label] + "</p>");
$("#myModal-title .modal-title").append(label);
$("#myModal-title").modal("toggle");
};

barCanvasYear.onclick = function(evt) {
  var activePoints =  barChartYear.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
    var label = activePoints[0]._model.label;
     } catch (error) {
    console.warn("Not interactive element was clicked");
    }
$("#myModal-year .modal-body").html("");
$("#myModal-year .modal-title").html("");
console.log(descrYear[label]);
$("#myModal-year .modal-body").append("<p>" + descrYear[label] + "</p>");
$("#myModal-year .modal-title").append(label);
$("#myModal-year").modal("toggle");
};

barCanvasSource.onclick = function(evt) {
  var activePoints =  barChartSource.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
    var label = activePoints[0]._model.label;
     } catch (error) {
    console.warn("Not interactive element was clicked");
    }
$("#myModal-source .modal-body").html("");
$("#myModal-source .modal-title").html("");
console.log(descrSource[label]);
$("#myModal-source .modal-body").append("<p>" + descrSource[label] + "</p>");
$("#myModal-source .modal-title").append(label);
$("#myModal-source").modal("toggle");
};

    window.onload = function () {
            window.abstractdiv = barChartAbstract;
            window.affiliationdiv = barChartAffiliation;
            window.authordiv = barChartAuthor;
            window.titlediv = barChartTitle;
            window.yeardiv = barChartYear;
            window.sourcediv = barChartSource;

        };
