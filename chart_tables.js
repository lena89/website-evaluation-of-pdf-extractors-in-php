var barCanvasTables = document.getElementById("bar-chart-tables");
var barCanvasCells = document.getElementById("bar-chart-cells");

Chart.defaults.global.defaultFontFamily = "Times New Roman";
Chart.defaults.global.defaultFontSize = 15;
Chart.defaults.global.defaultFontColor = "black";


var barDataTables = {
	labels: ['Tabula', 'Tabulizer', 'Tabula-Py'],
  datasets: [{
  	label: "Value",
		backgroundColor: "#ff5500",
    data: [1.00, 0.96, 0.35],
  }]
};

var descr = {
'Tabula': "Tabula is a tool for extracting tabular data into a CSV or Microsoft Excel spreadsheet using a simple, easy-to-use interface. Tabula is available as Java library and can be actually used as command-line jar file. But the GUI-oriented version which needs user interaction performs better since user can improve the results of detected tables. We tested the GUI-oriented version of Tabula, that is why it was not possible to integrate the test function into our website. On the official website of the tool you will find all the information you need. <p><a href='http://tabula.technology/'>Link to the website of the tool</a></p>",
'Tabulizer': "Tabulizer is an R package widely used by data scientists. It extracts tables from PDF into data frames. To see which output the library delivers, you can try the tool here online. Of course, tabulizer has more functionality, the simple function we offer shows a sample output. <p><a href='tabulizer.html'>Try the tool</a></p><p>To get more information about the tool please click on the link below.</p><p><a href='https://ropensci.org/tutorials/tabulizer_tutorial/'>Short tutorial on the tool</a></p> ",
'Tabula-Py': "Tabula-py is a Python library, so it cannot be used independently, but it is a very useful and simple in use in programming. It extracts tables from PDF into data frames. To try the tool and see the simple output click on the corresponding link below.</p> <p><a href='https://pypi.python.org/pypi/tabula-py'>More about the tool</a></p><p><a href='tabulapy.html'>Try the tool</a></p>",
};

var barDataCells = {
  labels: ['Tabula', 'Tabulizer', 'Tabula-Py'],
  datasets: [{
    label: "Value",
    backgroundColor: "#c45850",
    data: [0.74, 0.59, 0.26],
  }]
};


var barOptions = {
	legend: {
  display: false,
  position: 'top',
  labels: {
  	boxWidth: 80,
    fontColor: 'black'
    }
  },
  scales: {
    yAxes: [{
     scaleLabel: {
       display: true,
       labelString: 'Score'
     },
      ticks: {
        stepSize: 0.2,
        max: 1,
        min: 0
      }
    }],
    xAxes: [{
      ticks: {
        minRotation: 45,
        maxRotation: 45,
        barPercentage: 0.8,
        autoSkip: false
      }
    }]
  }
};

var barChartTables = new Chart(barCanvasTables, {
	type: 'bar',
  data: barDataTables,
  options: barOptions
});

var barChartCells = new Chart(barCanvasCells, {
  type: 'bar',
  data: barDataCells,
  options: barOptions
});

barCanvasTables.onclick = function(evt) {
	var activePoints =  barChartTables.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
      var label = activePoints[0]._model.label;
 } catch (error) {
       console.warn("Not interactive element was clicked");
    }
    $("#myModal-tables .modal-body").html("");
    $("#myModal-tables .modal-title").html("");
    console.log(descr[label]);
	            $("#myModal-tables .modal-body").append("<p>" + descr[label] + "</p>");
              $("#myModal-tables .modal-title").append(label);
              $("#myModal-tables").modal("toggle");
};

barCanvasCells.onclick = function(evt) {
	var activePoints =  barChartCells.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
      var label = activePoints[0]._model.label;
 } catch (error) {
       console.warn("Not interactive element was clicked");
    }
    $("#myModal-cells .modal-body").html("");
    $("#myModal-cells .modal-title").html("");
    console.log(descr[label]);
	            $("#myModal-cells .modal-body").append("<p>" + descr[label] + "</p>");
              $("#myModal-cells .modal-title").append(label);
              $("#myModal-cells").modal("toggle");
};

window.onload = function () {
        window.tablesdiv = barChartTables;
        window.cellsdiv = barChartCells;
};
