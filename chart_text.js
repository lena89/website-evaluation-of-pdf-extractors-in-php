var barCanvasWords = document.getElementById("bar-chart-words");
var barCanvasMis = document.getElementById("bar-chart-mis");
var barCanvasLev = document.getElementById("bar-chart-lev");

Chart.defaults.global.defaultFontFamily = "Times New Roman";
Chart.defaults.global.defaultFontSize = 15;
Chart.defaults.global.defaultFontColor = "black";

var barDataWords = {
	labels: ['Apache tika 1.13', 'Apache tika 1.14', 'Apache tika 1.15',  'Apache tika 1.16', 'Apache tika 1.17', 'pdftotext', 'Docsplit', 'Pdftools', 'Icecite', 'GROBID', 'PDFBox', 'DocToText'],
  datasets: [{
  	label: "Value",
		backgroundColor: "#3e95cd",
    data: [0.7480, 0.7463, 0.7461, 0.7463, 0.6989, 0.7447, 0.7447, 0.7378, 0.7998, 0.8942, 0.7443, 0.1199],
  }]
};

var barDataMis = {
  labels: ['Apache tika 1.13', 'Apache tika 1.14', 'Apache tika 1.15',  'Apache tika 1.16', 'Apache tika 1.17', 'pdftotext', 'Docsplit', 'Pdftools', 'Icecite', 'GROBID', 'PDFBox', 'DocToText'],
  datasets: [{
    label: "Value",
    backgroundColor: "#8e5ea2",
    data: [0.0015, 0.0016, 0.0016, 0.0019, 0.0015, 0.1418, 0.1418, 0.8584, 0.0, 0.20, 0.0015, 0.9815],
  }]
};

var barDataLev = {
  labels: ['Apache tika 1.13', 'Apache tika 1.14', 'Apache tika 1.15',  'Apache tika 1.16', 'Apache tika 1.17', 'pdftotext', 'Docsplit', 'Pdftools', 'Icecite', 'GROBID', 'PDFBox', 'DocToText'],
  datasets: [{
    label: "Value",
    backgroundColor: "#3cba9f",
    data: [0.7174, 0.7173, 0.7173, 0.7171, 0.7173, 0.6901, 0.6901, 0.5400, 0.6748, 0.5910, 0.7247, 0.1085],
  }]
};

var descr = {
'Apache tika 1.13': "Apache Tika is a toolkit developed for extracting text from more than a thousand different data types, which include PDF, PPT, XLS etc. Tika can be used as a command-line utility, as Maven dependency, in a Gradle-built project or in an Ant project. For the evaluation we used a command-line utility - a jar file. For extracting of text Apache Tika uses PDFBox technology.<p><a href='https://tika.apache.org/'>Link to the website of the tool</p> <p><a href='tika13.html' >Try Apache Tika 1.13</a></p>",
'Apache tika 1.14': "Apache Tika is a toolkit developed for extracting text from more than a thousand different data types, which include PDF, PPT, XLS etc. Tika can be used as a command-line utility, as Maven dependency, in a Gradle-built project or in an Ant project. For the evaluation we used a command-line utility - a jar file. For extracting of text Apache Tika uses PDFBox technology. <p><a href='https://tika.apache.org/'>Link to the website of the tool</p> <p><a href='tika14.html' >Try Apache Tika 1.14</a></p>",
'Apache tika 1.15': "Apache Tika is a toolkit developed for extracting text from more than a thousand different data types, which include PDF, PPT, XLS etc. Tika can be used as a command-line utility, as Maven dependency, in a Gradle-built project or in an Ant project. For the evaluation we used a command-line utility - a jar file. For extracting of text Apache Tika uses PDFBox technology. <p><a href='https://tika.apache.org/'>Link to the website of the tool</p> <p><a href='tika15.html' >Try Apache Tika 1.15</a></p>",
'Apache tika 1.16': "Apache Tika is a toolkit developed for extracting text from more than a thousand different data types, which include PDF, PPT, XLS etc. Tika can be used as a command-line utility, as Maven dependency, in a Gradle-built project or in an Ant project. For the evaluation we used a command-line utility - a jar file. For extracting of text Apache Tika uses PDFBox technology. <p><a href='https://tika.apache.org/'>Link to the website of the tool</p> <p><a href='tika16.html' >Try Apache Tika 1.16</a></p>",
'Apache tika 1.17': "Apache Tika is a toolkit developed for extracting text from more than a thousand different data types, which include PDF, PPT, XLS etc. Tika can be used as a command-line utility, as Maven dependency, in a Gradle-built project or in an Ant project. For the evaluation we used a command-line utility - a jar file. For extracting of text Apache Tika uses PDFBox technology. <p><a href='https://tika.apache.org/'>Link to the website of the tool</p> <p><a href='tika17.html' >Try Apache Tika 1.17</a></p>",
'pdftotext': "Xpdf-utils is an open-source package that contains command-line utilities such as pdftotext and pdfimages. We evaluated pdftotext utility, which you can try here. <p><a href='https://wiki.ubuntuusers.de/poppler-utils/'>More about the tool</a></p> <p><a href='pdftotext.html' >Try the tool</a></p>",
'Docsplit': "Docsplit is a command-line utility and Ruby library for splitting apart documents into their component parts: searchable UTF-8 plain text via OCR if necessary, page images or thumbnails in any format, PDFs, single pages, and document metadata (title, author, number of pages...) <p><a href='https://documentcloud.github.io/docsplit/' >Link to the website of the tool</a></p> <p><a href='pdfbox.html' >Try the tool</a></p>",
'Pdftools': "Pdftools is an R library, it is available as an R script and can be run from command-line. <p><a href='https://cran.r-project.org/web/packages/pdftools/pdftools.pdf' >More about the tool</a></p> <p><a href='pdftools.html' >Try the tool</a></p>",
'Icecite': "Icecite can be used as a command-line utility and also as a maven dependency for Java projects. You can try a command-line utility here. <p><a href='https://github.com/ckorzen/icecite' >Link to the github repository</a></p> <p><a href='icecite.html' >Try the tool</a></p>",
'GROBID': "GROBID is a machine learning library for extracting, parsing and re-structuring raw documents such as PDF into structured TEI-encoded documents with a particular focus on technical and scientific publications. After installation the most efficient way to use GROBID is the web service mode. You can also use the tool in batch mode or integrate it in your Java project via the Java API. To test the tool click on the corresponding link below. <p><a href='https://grobid.readthedocs.io/en/latest/'>More about the tool</a></p>",
'PDFBox': "Apache PDFBox is a JAVA library which includes several command-line utilities. One of the utilities - ExtractText - is aimed to extract text from PDF files. It is able to extract text in plain text and html formats. To test extracting in text format click on the corresponding link below. <p><a href='https://pdfbox.apache.org/' >Link to the website of the tool</a></p> <p><a href='pdfbox.html' >Try the tool</a></p>",
'DocToText': "DocToText is claimed to be a powerful tool for extracting text from PDF. But our evaluation showed a lot of empty output files, that is why we do not recommend this tool for text extraction from PDF. If you still would like to try it you can download it by the link below. <p><a href='https://sourceforge.net/projects/doctotext/'>More about the tool</a></p>"
}

var barOptions = {
	legend: {
  display: false,
  position: 'top',
  labels: {
  	boxWidth: 80,
    fontColor: 'black'
    }
  },
  scales: {
    yAxes: [{
      scaleLabel: {
       display: true,
       labelString: 'Score'
     },
       ticks: {
        max: 1,
        min: 0,
        stepSize: 0.2
      }
    }],
    xAxes: [{
      ticks: {
        autoSkip: false,
        minRotation: 45,
        maxRotation: 45,
        barPercentage: 0.8
      }
    }]
  }
};

var barChartWords = new Chart(barCanvasWords, {
	type: 'bar',
  data: barDataWords,
  options: barOptions
});

var barChartMis = new Chart(barCanvasMis, {
  type: 'bar',
  data: barDataMis,
  options: barOptions
});

var barChartLev = new Chart(barCanvasLev, {
  type: 'bar',
  data: barDataLev,
  options: barOptions
});

barCanvasWords.onclick = function(evt) {
      var activePoints = barChartWords.getElementsAtEvent(evt);
      console.log(activePoints);
      var group = activePoints[0]._chart.canvas.id;
      try {
           var label = activePoints[0]._model.label;
          } catch (error) {
            console.warn("Not interactive element was clicked!");

         }
         $("#myModal-words .modal-body").html("");
         $("#myModal-words .modal-title").html("");
         console.log(descr[label]);
                         $("#myModal-words .modal-body").append("<p id='modal'>" + descr[label] + "</p>");
                         $("#myModal-words .modal-title").append(label);       
$("#myModal-words").modal("toggle");
       };

barCanvasMis.onclick = function(evt) {
	var activePoints =  barChartMis.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
      var label = activePoints[0]._model.label;
 } catch (error) {
       console.warn("Not interactive element was clicked");

    }
    $("#myModal-mis .modal-body").html("");
    $("#myModal-mis .modal-title").html("");
    console.log(descr[label]);
	            $("#myModal-mis .modal-body").append("<p id='modal'>" + descr[label] + "</p>");
                    $("#myModal-mis .modal-title").append(label);
              $("#myModal-mis").modal("toggle");
};

barCanvasLev.onclick = function(evt) {
	var activePoints =  barChartLev.getElementsAtEvent(evt);
  console.log(activePoints);
  var group = activePoints[0]._chart.canvas.id;
  try {
      var label = activePoints[0]._model.label;
 } catch (error) {
       console.warn("Not interactive element was clicked");

    }
    $("#myModal-lev .modal-body").html("");
    $("#myModal-lev .modal-title").html("");
    console.log(descr[label]);
	            $("#myModal-lev .modal-body").append("<p id='modal'>" + descr[label] + "</p>");
                    $("#myModal-lev .modal-title").append(label);
              $("#myModal-lev").modal("toggle");
};

window.onload = function () {
        window.wordsdiv = barChartWords;
        window.misdiv = barChartMis;
        window.levdiv = barChartLev;
};
