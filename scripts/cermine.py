#!/usr/bin/env python3
import subprocess
import os
import glob
import shutil


# output path is the
def extract_pdf_cermine(tool_path, pdf_path):
    process = subprocess.Popen(
        'java -cp ' + tool_path + ' pl.edu.icm.cermine.ContentExtractor -path ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()


def move_output_files(path_one, path_two):
    files_names = os.listdir(path_one)
    for i in range(0, len(files_names)):
        if files_names[i].endswith(".cermxml"):
            shutil.move(path_one + "/" + files_names[i], path_two)


def leave_the_latest(pdf_directory):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    for i in range(1, len(array)):
        os.remove(array[i])


if __name__ == "__main__":
    leave_the_latest("/var/www/html/uploads")
    extract_pdf_cermine("/var/www/html/tools/cermine/cermine-impl-1.13-jar-with-dependencies.jar",
                       "/var/www/html/uploads")
    move_output_files("/var/www/html/uploads",
                       "/var/www/html/downloads")
