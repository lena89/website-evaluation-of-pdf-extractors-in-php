#!/usr/bin/python
import subprocess
import os
import glob


def extract_pdf_docsplit(pdf_path, save_path):
    process = subprocess.Popen(
        'cd ' + save_path + ' && docsplit text ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    process_out, process_err = process.communicate()


def extract_the_latest(pdf_directory, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_docsplit(array[0], output_path)


if __name__ == "__main__":
    extract_the_latest("/var/www/html/uploads",
                       "/var/www/html/downloads")
