#!/usr/bin/env python3
import subprocess
import os
import glob
import shutil


def extract_pdf_grobid(tool_path, pdf_path):
    process = subprocess.Popen(
        'cd ' + tool_path + ' && java -Xmx1G -jar grobid-core/build/libs/grobid-core-0.5.1-onejar.jar -gH grobid-home -dIn ' + pdf_path + ' -r -exe processHeader',
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()


def move_output_files(path_one, path_two):
    files_names = os.listdir(path_one)
    for i in range(0, len(files_names)):
        if files_names[i].endswith(".tei.xml"):
            shutil.move(path_one + "/" + files_names[i], path_two)


def leave_the_latest(pdf_directory):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    for i in range(1, len(array)):
        os.remove(array[i])


if __name__ == "__main__":
    #eave_the_latest("/home/yelena/website-evaluation-of-pdf-extractors-in-php/uploads")
    extract_pdf_grobid("/home/yelena/grobid-0.5.1",
                       "/home/yelena/website-evaluation-of-pdf-extractors-in-php/uploads")
    move_output_files("/home/yelena/grobid-0.5.1",
                       "/home/yelena/website-evaluation-of-pdf-extractors-in-php/downloads")
