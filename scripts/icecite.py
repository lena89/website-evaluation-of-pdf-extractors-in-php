#!/usr/bin/env python3
import subprocess
import os
import glob
import shutil


# output path is the
def extract_pdf_icecite(tool_path, pdf_path):
    process = subprocess.Popen(
        'java -jar ' + tool_path + ' --format json ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()


def extract_the_latest(pdf_directory, tool_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_icecite(tool_path, array[0])


def move_output_files(path_one, path_two):
    files_names = os.listdir(path_one)
    for i in range(0, len(files_names)):
        if files_names[i].endswith("json"):
            shutil.move(path_one + "/" + files_names[i], path_two)


if __name__ == "__main__":
    extract_the_latest("/var/www/html/uploads",
                       "/var/www/html/tools/icecite/icecite-pdf-parser.jar")
    move_output_files("/var/www/html/uploads",
                      "/var/www/html/downloads")
