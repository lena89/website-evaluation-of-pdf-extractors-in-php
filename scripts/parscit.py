#!/usr/bin/env python3
import subprocess
import os
import glob
from bs4 import BeautifulSoup


# firstly convert pdf to text, because parscit is only able to process .txt files
# save text file into temp dir
def convert_to_text(pdf_path, output_path):
    complete_name = os.path.join(output_path, os.path.basename(pdf_path) + '.txt')
    process = subprocess.Popen(
        'pdftotext ' + pdf_path + ' ' + complete_name,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    return complete_name


def extract_pdf_parscit(tool_path, pdf_path, output_path, xml_path):
    text_path = convert_to_text(pdf_path, output_path)
    process = subprocess.Popen(
        'cd ' + tool_path + ' && ./citeExtract.pl -m extract_all ' + text_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    xml_path = xml_path + "/" + os.path.basename(pdf_path) + ".txt"
    with open(xml_path, "wb") as text_file:
        text_file.write(process_out)


def extract_the_latest(tool_path, pdf_directory, output_path, xml_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_parscit(tool_path, array[0], output_path, xml_path)


if __name__ == "__main__":
    extract_the_latest("/home/ubuntu/ParsCit/bin",
                       "/var/www/html/uploads",
                       "/var/www/html/temp",
                       "/var/www/html/downloads")
