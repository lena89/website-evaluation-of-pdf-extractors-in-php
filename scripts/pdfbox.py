#!/usr/bin/env python3
import subprocess
import os
import glob


# output path is the
def extract_pdf_pdfbox(tool_path, pdf_path, save_path):
    complete_name = os.path.join(save_path, os.path.basename(pdf_path) + '.txt')
    process = subprocess.Popen(
        'java -jar ' + tool_path + ' ExtractText ' + pdf_path + ' ' + complete_name,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()


def extract_the_latest(pdf_directory, tool_path, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_pdfbox(tool_path, array[0], output_path)


if __name__ == "__main__":
    extract_the_latest("/var/www/html/uploads",
                       "/var/www/html/tools/pdfbox/pdfbox-app-2.0.8.jar",
                       "/var/www/html/downloads")
