#!/usr/bin/python
import subprocess
import os
import glob


def extract_pdf_pdftools(tool_path, pdf_path, save_path):
    complete_path = os.path.join(save_path, os.path.basename(pdf_path) + '.txt')
    process = subprocess.Popen(
        'Rscript --vanilla ' + tool_path + ' -i ' + pdf_path + ' -o ' + complete_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    process_out, process_err = process.communicate()


def extract_the_latest(tool_path, pdf_directory, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_pdftools(tool_path, array[0], output_path)


if __name__ == "__main__":
    extract_the_latest("/home/yelena/website-evaluation-of-pdf-extractors-in-php/tools/pdftools/extract.R",
                       "/home/yelena/website-evaluation-of-pdf-extractors-in-php/uploads",
                       "/home/yelena/website-evaluation-of-pdf-extractors-in-php/downloads")
