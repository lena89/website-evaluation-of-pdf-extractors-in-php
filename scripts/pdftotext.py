#!/usr/bin/python
import subprocess
import os
import glob


def extract_pdf_pdftotext(pdf_path, save_path):
    complete_name = os.path.join(save_path, os.path.basename(pdf_path) + '.txt')
    process = subprocess.Popen(
        'pdftotext ' + pdf_path + ' ' + complete_name,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()


def extract_the_latest(pdf_directory, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_pdftotext(array[0], output_path)
    

if __name__ == "__main__":
    extract_the_latest("/var/www/html/uploads",
                       "/var/www/html/downloads")
