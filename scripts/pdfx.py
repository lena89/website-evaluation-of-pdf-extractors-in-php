#!/usr/bin/python
import subprocess
import os
import glob


# output path is the
def extract_pdf_pdfx(pdf_path, save_path):
    complete_path = os.path.join(save_path, os.path.basename(pdf_path) + '.txt')
    process = subprocess.Popen(
        'pdfx --text ' + pdf_path + ' -o ' + complete_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    process_out, process_err = process.communicate()


def extract_the_latest(pdf_directory, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    print(array[0])
    extract_pdf_pdfx(array[0], output_path)


if __name__ == "__main__":
    extract_the_latest("/opt/lampp/htdocs/website/uploads",
                       "/opt/lampp/htdocs/website/downloads")
