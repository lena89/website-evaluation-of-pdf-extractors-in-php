#!/usr/bin/env python3
import subprocess
import os
import glob
from bs4 import BeautifulSoup

# supposed that server.py has been started successfully
def extract_pdf_svm(pdf_path, output_path):
    process = subprocess.Popen(
        'curl --form "myfile=@' + pdf_path +'" http://localhost:8080/extractor',
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    soup = BeautifulSoup(process_out, 'html.parser')
    header = soup.find("header").text
    process_two = subprocess.Popen(
        'curl ' + header,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process_two.communicate()
    print(process_out)
    output_path = output_path + "/" + os.path.basename(pdf_path) + ".txt"
    with open(output_path, "wb") as text_file:
        text_file.write(process_out)


def extract_the_latest(pdf_directory, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    extract_pdf_svm(array[0], output_path)


if __name__ == "__main__":
    extract_pdf_svm("/home/yelena/collection_for_manual_metadata_evaluation/pdf/astro-ph0001004.pdf",
                    "/home/yelena/collection_for_manual_metadata_evaluation/pdf")
