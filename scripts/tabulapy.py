#!/usr/bin/env python3
import subprocess
import os
import glob
import re
from tabula import read_pdf
import numpy as np


def extract_tabulapy(pdf_path, output_path):
    df = read_pdf(pdf_path)
    #print(str(df))
    complete_name = os.path.join(output_path, os.path.basename(pdf_path) + '.txt')
    #np.savetxt(complete_name, df.values, fmt='%d')
    file = open(complete_name, 'wt', encoding='utf-8')
    file.write(str(df))


def extract_the_latest(pdf_directory, output_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    print(array[0])
    extract_tabulapy(array[0], output_path)


if __name__ == "__main__":
    extract_the_latest("/home/yelena/website-evaluation-of-pdf-extractors-in-php/uploads",
                       "/home/yelena/website-evaluation-of-pdf-extractors-in-php/downloads")
