#!/usr/bin/env python3
import subprocess
import os
import glob
import re


def extract_tabulizer(pdf_path, output_path, tool_path):
    process = subprocess.Popen(
        'Rscript --vanilla ' + tool_path + ' ' + pdf_path,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True)
    process_out, process_err = process.communicate()
    complete_name = os.path.join(output_path, os.path.basename(pdf_path) + '.txt')
    process_out_string = process_out.decode('utf-8')
    file = open(complete_name, 'wt', encoding='utf-8')
    file.write(process_out_string)


def extract_the_latest(pdf_directory, output_path, tool_path):
    array = glob.glob(pdf_directory + "/*.pdf")
    array.sort(key=os.path.getmtime, reverse=True)
    print(array[0])
    extract_tabulizer(array[0], output_path, tool_path)


if __name__ == "__main__":
    extract_the_latest("/var/www/html/uploads",
                       "/var/www/html/downloads",
                       "/var/www/html/tools/tabulizer/tabulizer.R")
