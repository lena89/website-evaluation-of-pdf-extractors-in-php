<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Always force latest IE rendering engine or request Chrome Frame -->
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet"
    href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>PDF uploaded</title>
    </head>
    <body>
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="index.html">Evaluation of Data Extraction from PDF</a>
          </div>
      <ul class="nav navbar-nav">
  <li><a href="index.html">Home</a></li>
  <li><a href="tools.html">Tools</a></li>
  <li><a href="about.html">About</a></li>
  </ul>
  </div>
  </nav>
  <div class="container-fluid">
  </div>
<?php
 $targetfolder = "/var/www/html/uploads/";
 $targetfolder = $targetfolder . basename( $_FILES['fileToUpload']['name']) ;
if(move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $targetfolder))
 {
 echo "<p>The file ". basename( $_FILES['fileToUpload']['name']). " is uploaded</p>";
 }
 else {
 echo $targetfolder;
 echo "<p>Problem uploading file</p>";}
 //convert the uploaded file with a script into a text file and store it
exec("python3 /var/www/html/scripts/tika15.py", $output);
 ?>
 <form>
 <input type="button" value="Download text file"
 onclick="window.location.href='download.php'"/>
</form>
    </body>
</html>
