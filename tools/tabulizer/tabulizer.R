#!/usr/bin/env Rscript
args = commandArgs(trailingOnly = TRUE)
if (length(args)==0) {
  stop("At least one argument must be supplied", call. = FALSE)
}
library(tabulizer)
file <- args[1]
tab <- extract_tables(file)
write(str(tab), stdout())

