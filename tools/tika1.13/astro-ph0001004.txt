Asymmetrical Planetary Nebulae II: From Origins to Microstructures
ASP Conference Series, Vol. 199, 2000
J.H. Kastner, N. Soker, & S. Rappaport, eds.

Theory of the Interaction of Planetary Nebulae with the
Interstellar Medium
Ruth Dgani
Department of Astronomy, The University of Texas at Austin, Austin
TX 78712
Abstract. The theory of the interaction of planetary nebulae with the
interstellar medium is important for the interpretation of nebular morphologies that deviate from point symmetry. It can be used to probe the
interstellar medium and its magnetic field. We emphasize in this review
the role of hydrodynamical instabilities in the interaction.

1.

Introduction

The interaction of planetary nebulae (PNs) with the interstellar medium (ISM)
was first discussed by Gurzadyan (1969). Smith (1976) used the thin shell model
to analyze the evolution of a spherically symmetric expanding PN shell moving
through the ISM (see also Isaacman 1979). On the observational side, only
a few PNs showed interaction with the ISM (hereafter refereed to as interacting PNs) until the last decade. With better observational imaging instruments
(CCD cameras) the field interacting PNs became more popular (e.g., Borkowski,
Tsvetanov, & Harrington 1993; Jacoby & Van de Steene 1995; Tweedy, Martos
& Noriega-Crespo 1995; Hollis et al. 1996; Zucker & Soker 1997). Borkowski,
Sarazin & Soker (1990;hereafter BSS) discuss the potential in using interacting
PNs to probe the ISM, and list many interacting and possibly interacting PNs.
This list was extended in the review article by Tweedy (1995) and in the atlas of
interacting PNs (Tweedy & Kwitter 1996). Tweedy with several collaborators
has been leading the observational research in the field (e.g., Tweedy & Kwitter 1994a,b; Tweedy & Napiwotzki 1994). Other studies are in progress, and
preliminary results were presented in the conference (e.g. Kerber et al. 2000;
Dopita et al. 2000 ).
On the theoretical side, Soker, Borkowski & Sarazin (1991;hereafter SBS)
performed numerical simulations, both in the isothermal and adiabatic limits.
Their results validate the thin-shell approximation of Smith (1976). In the adiabatic case they found that the Rayliegh-Taylor (RT) instability plays a crucial
role in the evolution. Recent numerical calculations reported in this conference (Dopita et al. 2000; Villaver et al. 2000) confirmed these results. In their
observational analysis of the interacting PN IC 4593, Zucker & Soker (1993) suggested that the interaction process oscillates between the adiabatic and isothermal cases. An analytical study of this process, the so-called radiative shock
overstability, was performed by Dgani & Soker (1994) for finite-sized objects
(e.g., spherical nebulae). The radiative overstability was found to occur only
1

2

Ruth Dgani

19 −1
in large PNs, with radii of R >
∼ 10 n0 cm, where n0 is the total number density of the ISM (in cm−3 ) The instability occurs only for large PNs because the
transverse flow, here it is the flow around the nebula, stabilizes the radiative
overstability (Dgani & Soker 1994; see also Stevens, Blondin & Pollock 1992, for
a numerical study of a different situation). For an earlier review of the theory
see Dgani(1995). Several recent theoretical studies were conducted since that
review. Soker & Dgani (1997) studied analytically the interaction of PNs with
a magnetized ISM. Dgani & Soker (1998) and Dgani(1998) discussed the role of
instabilities in the interaction. New numerical studies are underway (Villaver et
al. 2000; Dopita et al. 2000), preliminary results presented in the conference
show that instabilities play an important role in the interaction.

2.
2.1.

Preliminary Considerations
Why is the interaction important?

The interaction of PNs with the ISM causes deviation form axisymmetry (or
point symmetry). Several other processes that cause deviation from axisymmetry involving binaries were discussed by Soker and collaborators (e.g. Soker
1994; Soker 1996; Soker et al 1998).
It is important to understand as accurately as possible the ISM effects on
the morphology and to distinguish them from other processes.
Interacting PNs are also a tool for studying the ISM. This role was emphasized in previous reviews (Tweedy 1995; Dgani 1995).
2.2.

Detecting the interaction

The interaction can be divided into at least three phases: free expansion, deceleration and stripping off the central star. Initially, PNs expand freely because
their densities are of orders of magnitude higher than the densities of their surrounding medium. Eventually, though, every nebula will reach a stage in which
the pressure inside the nebula is of the same order as the pressure outside the
nebula. A strong enough pressure wave will then propagate into the nebula and
decelerate it. If the nebula moves supersonically, the dominant ISM pressure is
the ram pressure. The critical density ncrit , at which the ISM and the nebula
reach the same pressure is (BSS, Eq. 1):


ncrit =

v∗ + ve
c

2

n0 ,

(1)

where c is the isothermal sound speed in the nebula, c ∼ 10 km s−1 in most cases,
v∗ is the velocity of the central star, ve is the expansion velocity of the nebula
and n0 is the ISM density. The most difficult parameter to observe directly in
the above formula is the ISM density, n0 . The nebula at this early stage of the
interaction is not distorted, but its leading edge is brighter. Measuring the value
of the density at the leading edge, ncrit , and using the above formula yields an
estimate for n0 .
If we take the average value of ve = 20 km s−1 (Weinberger 1989) and v∗ =
60 km s−1 (Pottash 1984; BSS), then ncrit = 60n0 . As the ISM ram pressure

Interaction of Planetary Nebulae with the Interstellar Medium

3

depends on the velocity squared, the interaction is much stronger for fast moving
planetaries. For example, if v∗ = 150 km s−1 then the critical density is about
300 times that of the ISM.
The electron number density of extended planetaries is about 100 cm−3 (Kaler
1983) for the low surface brightness PNs from the NGC catalog, and 10 cm−3
for the fainter PNs from the Abell (1966) catalog. The very large nebula S-216
has ne =5 cm−3 . In principle, therefore, fast moving planetary nebulae can be
detected in very low density environments n0 ' 0.01.
3.

Numerical Models

The first realistic models of moving PNs were calculated numerically by SBS.
They performed hydrodynamic simulations of the PN-ISM interaction with the
particle in cell (PIC) method in cylindrical symmetry. They calculated two types
of models of thick nebular shells moving through the ISM: an adiabatic model
which applies to fast nebulae moving in the galactic halo and an isothermal
model relevant to average nebulae moving in the galactic plane.
3.1.

Cooling in the ISM shock

A nebula moving supersonically in the ISM creates a strong bow shock before
it. The cooling time of the shocked ISM gas depends on the ionization state of
the gas before the shock. The following analytical formula for the cooling time
is based on numerical calculations of a steady shock with ionization precursor.
It is accurate within a factor of 2 for vs >60 km s−1 Mckee (1987):
vs
= 100
50 km/s


tcool

3

n−1
0 yr.

(2)

In steady state shocks with velocities of about 50 km s−1 , the cooling drops by
a factor of 10 when the material is preionized (Raymond 1979). However, the
cooling time is still shorter than the flow time for an average nebula moving in
the galactic plane. Only when the relative velocity is high and the density is
low, will an adiabatic flow appear.
3.2.

The adiabatic model

SBS choose the parameters for the adiabatic model so that the cooling time is
larger than the flow time. An instability appears near the axis while the shell
is decelerating which is interpreted as a Rayleigh-Taylor instability in the decelerated shell. When the shocked ISM cannot cool, the deceleration of the dense
nebular shell by the dilute shocked ISM is Rayleigh-Taylor unstable. The main
morphological features of the adiabatic flow are the ISM shock front, the decelerated PN shell and the RT instability. The last manifests itself as a ”bump” and
a hole in the nebula along the upstream symmetry axis. The numerical scheme
used by SBS, though, being of 2D cylindrically symmetric nature and limited
spatial resolution, forces the fastest growing mode to be of large wavelength and
near the symmetry axis. Similar instability modes are obtained in numerical
simulations of other types of dense bodies moving through an ambient media
(Brighenti & D’Ercole 1995 for Wolf-Rayet ring nebulae, and Jones, Hyesung &

4

Ruth Dgani

Tregillis 1994 for dense gas clouds). Brighenti & D’Ercole (1995) present a test
run where the numerical grid is Cartesian rather than cylindrical. They find
similar fragmentation of the flow, but not along the symmetry axis.
3.3.

The isothermal model

In the isothermal model, efficient cooling is assumed. The cooling is calculated
in the following way: for every cell in which the temperature is more than 104 K
the temperature is set equal to 104 K. The model parameters are typical for an
average nebula moving in the galactic plane.
In this case the shocked region is thin and ”arms” of denser ISM material follow
the nebula. The Rayleigh-Taylor instability does not appear in this case. This
is because the shocked ISM is cooled and its density is of the same order as the
nebular density. The model agrees with the thin shell approximation of Smith.
3.4.

Recent numerical simulations.

Numerical simulations show that several types of instabilities which develop on
the interface of spherically expanding shells (or winds) moving with respect to
the ISM can fragment the shells (e.g. Brighenti & D’Ercole 1995 ). The detailed
2D numerical simulations of Brighenti & D’Ercole (1995) nicely show how RT
and KH instabilities on the interface of the wind and the ISM develop, and allow
the ISM to penetrate into the wind-bubble.
New numerical studies of interacting PNs are underway (Villaver et al.
2000; Dopita et al. 2000). Preliminary results presented in the conference show
that the RT instabilities can fragment the nebular shell and allow the ISM to
penetrate to the inner parts of the nebulae in some cases.
4.

The Role of the ISM Magnetic Field in Interacting PNs

The important role of the ISM magnetic field on shaping interacting PNs was
first mentioned in the observational paper of Tweedy et al (1995) about the
nebula Sh 216. The morphology of the nebula is dominated by a bow shock
followed by several parallel elongated filaments. Tweedy et al. (1995) noted
that the magnetic pressure of the ISM is negligible compared to its ram pressure
for the assumed central star velocity (v∗ ∼ 20 km s−1 ). They have argued that
the central star velocity must be very low i.e. v∗ ∼ 5 km s−1 in order to explain
the magnetic shaping of the nebula. Dgani & Soker (1998; table 1) noted that
too many interacting PNs (a dozen) show signs of magnetic shaping i.e. parallel
elongated filaments. It is very unlikely that all of them have such abnormally
low central star velocity.
4.1.

General features

Soker & Dgani (1997) conduct a theoretical study of the processes involved when
the ISM magnetic field is important in the interaction. In the case where the
ISM is fully ionized, we define four characterizing velocities of the interaction
process: the adiabatic sound speed vs = (γkT /µmH ) and the Alfven velocity
vA = B0 /(4πρ0 )1/2 of the ISM, the expansion velocity of the nebula ve , and the
relative velocity of the PN central star and the ISM v∗ . The interesting cases,

Interaction of Planetary Nebulae with the Interstellar Medium

5

with the magnetic field lines being at a large angle to the relative velocity direction, are:

1. v∗  vA ∼ vs ∼ ve , and a rapid cooling behind the shock wave. Both
the thermal and magnetic pressure increase substantially behind a strong
shock. If radiative cooling is rapid, however, the magnetic pressure will
eventually substantially exceed the thermal pressure, leading to several
strong MHD instabilities around the nebula, and probably to magnetic
field reconnection behind the nebula.
2. v∗  vA ∼ vs ∼ ve , and negligible cooling behind the shock. The thermal
pressure, which grows more than the magnetic pressure in a strong shock,
will dominate behind the shock. Magnetic field reconnection is not likely
to occur behind the nebula. This domain characterizes the interaction of
the solar wind with the atmospheres of Venus and Mars (e.g., Phillips &
McComas 1991).
In the ISM it is likely that vs ∼ vA , and ve ' 10 km s−1 will in most cases
not exceed the ISM sound speed. The central star velocity v∗  vs for most
cases. Case (1) is the magnetic parallel of the isothermal model (section 3.1)
which is relevant for planetary nebulae moving in the galactic plane. Case (2)
is relevant for galactic halo PNs..
4.2.

The magnetic Rayliegh-Taylor instability for galactic plane PNs

The isothermal case (case 1) is the more typical for the majority of PNs, moving
moderately fast (v∗ ∼ 60 km/s) close to the galactic plain, where the ISM
density is relatively high. Soker et al. (1991) find in their numerical simulations
that the isothermal model shows no sign of the RT instability. Soker & Dgani
(1997) show the ISM magnetic field, which was not incorporated in the numerical
simulations of Soker et al. (1991), can lead to an interesting manifestation of
the RT instability in the isothermal case (case 1). We assume that there is a
magnetic field of intensity Bs in the shocked ISM, but not in the nebula, and
that its field lines are parallel to the boundary separating the ISM and nebular
material. The acceleration g (deceleration in the frame of the central star) of the
nebular front (pointing downstream) is in the opposite direction to the density
gradient. In the linear regime the growth rate of the RT instability, σ, with a
wavenumber k = 2π/λ, is given by (e.g., Priest 1984; §7.5.2)
σ 2 = −gk

ρn − ρI
Bs2
+
k 2 cos2 θ,
ρn + ρI
4π(ρn + ρI )

(3)

where ρI and ρn are the ISM and nebular density, respectively, θ is the angle
between the magnetic field lines and the wave-vector, and RT instability occurs
when σ 2 < 0. The deceleration of the leading shell by the ISM ram pressure is
given by
g ∼ πR2 ρ0 v∗2 /MF ,

(4)

6

Ruth Dgani

where R is the nebular radius, and MF is the mass being decelerated. Taking
R = 0.5pc, v∗ = 60 km s−1 , ρ0 = 10−25 g cm−3 and MF = 0.05M we obtain
g ∼ 3 × 10−7 cm s−2 .
We assume that after it is shocked the nebular material reaches 104 K, and
2 is approximately equal to the ram pressure of
that its thermal pressure ρn vsn
2
the ISM, ρ0 v∗ . Here ρn is the shocked nebular density, vsn ' 10 km s−1 is the
isothermal sound speed in the shocked cooled nebula, and we use the assumption
v∗  ve . The post-shock nebular density is therefore
2
ρn = ρ0 v∗2 /vsn
.

(5)

RT instability occurs when the density of the shocked cooled ISM ρs is
smaller than the post-shock nebular density. This can happened for a magnetic
shock when the magnetic pressure provides the support for the shocked ISM
region.For a fast enough growth of RT instability we require ρs /ρn < 1/3. Assuming equipartition in the undisturbed ISM, we find the condition for a strong
enough RT. Namely, we find that for a typical PN velocity v∗ = 60 km s−1
through the ISM the pre-shocked ISM magnetic field line should be within 45◦
of the shock front direction. Therefore, in most typical cases, i.e., equipartition
and v∗ ∼ 60 km s−1 , fast growing RT instability modes with long wavelengths
will develop.
The magnetic field has a destabilizing effect only for modes having wave
numbers close to being perpendicular to the field lines (and in the plane separating the ISM and the nebula). Instability modes having wave numbers along
the field lines are suppressed by the magnetic tension. This effect may create
elongated structures in the direction of the magnetic field lines.
4.3.

The morphology of interacting PNs and their galactic latitude

Dgani & Soker (1998) explore the observational consequences of the development
of RT in galactic plane PNs moving in a magnetized ISM. In order to define the
galactic plane and galactic halo population we use the expression for the cooling
time after the ISM shock (eq. 2). The cooling will be effective when the flow
time of shocked ISM around the nebula tf low ∼ R/v∗ is longer than the cooling
time. Calling the ratio of the two times η, we obtain:
v∗
∼ 0.1
50 km s−1


η = tcool /tf low

4 

R
18
10 cm

−1 

n0
0.1cm−3

−1

(6)

−1
Halo PNs are expected to have a speed of >
∼ 100 km s through the ISM, while
those in the plane move much slower. In addition, close to the galactic plane the
density is higher than in the halo. We therefore expect that halo PNs will have
η > 1 and therefore the flow will be adiabatic, while for disk PNs η < 1 and the
flow will be isothermal. Several high quality observations, most notably given in
the Atlas published by Tweedy & Kwitter (1996) provide us with a sample of 34
deep images of interacting PNs. In table 1 of Dgani & Soker (1998), we give a list
of the objects and their references. Some nebulae have several parallel stripes.
We define a nebula as striped only if it has at least two parallel long filaments.
All of the 12 striped nebulae are close to the galactic plane (|z| <250 pc). The
fact that the striped nebulae are confined to the galactic plane is compatible with

Interaction of Planetary Nebulae with the Interstellar Medium

7

the the prediction that RT will be effective for galactic plane PNs moving in the
warm medium, because of magnetic effects, but only for modes perpendicular
to the direction of the magnetic field. The result is elongated structures or ”RT
rolls”. For galactic halo interacting PNs the cooling is inefficient and magnetic
pressure is negligible. RT will be effective but it will form fingers or blobs and
not stripes.
4.4.

Measuring the ISM magnetic field with interacting PNs

The group of 12 nebulae that show stripes is not homogeneous; some have thick
loose stripes, some have thin densely packed stripes. In Dgani (1998) the properties of the striped nebulae and their application to the study of the ISM are
explored. In particular, a simple model for the shocked ISM region is used to
derive a relation between the distance between adjacent stripes and the strength
of the magnetic field of the ISM. If ∆z is the average spacing of the stripes in
units of the radius of the nebula then (see Dgani 1997, eq. 4):
∆z ∼ vA0 cos α0 /v∗ ,

(7)

where vA0 is the Alfven speed in the ISM, α0 is the angle between the pre-shock
magnetic field and the shock front, and v∗ is the velocity of the central star. According to the formula nebulae with densely packed stripes either move faster or
move in a weaker magnetic field than the ones with thicker looser stripes. Applying this simple formula to several striped nebulae, Dgani shows that information
about the strength of the ISM magnetic field in the local neighborhood of these
nebulae can be extracted by accurate observations of the members of this group.
Acknowledgement: It is a pleasure to thank Noam Soker for introducing me to the fascinating subject of interacting nebulae and for a very pleasant
collaboration.
References
Abell, G. O. 1966, ApJ, 144, 259.
Borkowski, K. J., & Tsvetanov, Z., & Harrington, J. P. 1993, ApJ Lett., 402,
L57.
Borkowski, K. J., & Sarazin, C. L., & Soker, N. 1990, ApJ, 360, 173 (BSS).
Brighenti, F. & D’Ercole, A. 1995, MNRAS, 277, 53.
Dgani, R. 1995, Ann. of the Israel Physical Society, Vol. 11: Asymmetrical
Planetary Nebulae, eds. A. Harpaz and N. Soker (Haifa, Israel), p. 219.
Dgani, R. 1998, RevMexAC, 7, 149.
Dgani, R., & Soker, N. 1994, ApJ, 434, 262.
Dgani, R., & Soker, N. 1997, ApJ, 495, 337.
Dopita, M. et. al., 2000, these proceedings.
Gurzadyan, G. A. 1969, Planetary Nebulae (New York : Gordon & Breach), p.
235.

8

Ruth Dgani

Hollis, J. M., Van Buren, D., Vogel, S. N., Feibelman, W. A., Jacoby, G. H., &
Pedelty, J. A. 1996, ApJ, 456, 644.
Isaacman, R. 1979, A&A, 77, 327.
Jacoby, G. H., & Van de Steene, G. 1995, AJ, 110, 1285.
Jones, T. W., Kang, H., & Tregillis, I. L. 1994, ApJ, 432, 194.
Kaler J. B. 1983, ApJ, 271, 188.
Kerber et. al. 2000, these proceedings.
McKee, C. F. 1987 in Spectroscopy of Astrophysical Plasmas, ed. A. Dalgarno
Pottash, S. R. 1984, Planetary Nebulae (Dordrecht : Reidel).
Priest, E. R. 1984, Solar Magnetohydrodynamics, Reidel Pub. Com., (Dordrecht, holland).
Raymond, J. C. 1979, ApJS, 39, 1.
Smith, H. 1976, MNRAS, 175, 419.
Soker, N.., 1994, MNRAS, 270, 774.
Soker, N.., 1996, ApJ, 496, 734. .
Soker, N.. & Dgani, R., 1997, ApJ, 484, 277.
Soker, N., Borkowski, K. J., & Sarazin, C. L. 1991, AJ, 102, 1381 (SBS).
Soker, N., Rappaport, S., & Harpaz, A., 1998, ApJ. 496, 842.
Stevens, I. R., Blondin, J. M., & Pollock, A. M, 1992, ApJ, 386, 265.
Tweedy, R. W. 1995, Ann. of the Israel Physical Society, Vol. 11: Asymmetrical
Planetary Nebulae, eds. A. Harpaz and N. Soker (Haifa, Israel), p. 209.
Tweedy, R. W., & Kwitter, K. B. 1994a, AJ 108, 188.
Tweedy, R. W., & Kwitter, K. B. 1994b, ApJ 433, L93.
Tweedy, R. W., & Kwitter, K. B. 1996, ApJS 107, 255.
Tweedy, R. W., Martos, M. A., & Noriega-Crespo, A. 1995, ApJ, 447, 257.
Tweedy, R. W., & Napiwotzki, R. 1994, AJ 108, 978.
Villaver, E. et. al. these proceedings.
Weinberger, R. 1989, A&A Supp., 78, 301.
Zucker, D. B. & Soker, N. 1993, ApJ, 408, 579.
Zucker, D. B. & Soker, N. 1997, MNRAS, 289,665.

